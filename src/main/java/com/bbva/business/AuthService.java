package com.bbva.business;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;

/**
 * Created by sc05266 on 10/16/2017.
 */
public interface AuthService {

    Future<User> getAccessToken(String clientId, String code, String redirectUri);
    Future<JsonObject> getUser(User token);
    Future<JsonObject> getContact(User token, JsonObject user);
    Future<String> getJWTToken(JsonObject user, JsonObject contact);
}
