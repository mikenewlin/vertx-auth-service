package com.bbva.business.impl;

import com.bbva.business.AuthService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.auth.oauth2.providers.GoogleAuth;

import javax.inject.Inject;

/**
 * Created by sc05266 on 10/16/2017.
 */
public class AuthServiceImpl implements AuthService {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    private Vertx vertx;
    private JsonObject config;
    private HttpClient client;
    private JWTAuth jwtAuth;

    @Inject
    public AuthServiceImpl(Vertx vertx, JsonObject config) {
        this.vertx = vertx;
        this.config = config;
        HttpClientOptions options = new HttpClientOptions().setSsl(true);
//                .setProxyOptions(new ProxyOptions().setType(ProxyType.HTTP).setHost("10.10.126.46").setPort(8080).setUsername("sc05266"));
        client = vertx.createHttpClient(options);
//        JsonObject jwtConfig = new JsonObject().put("keyStore", new JsonObject()
//                .put("path", "jwtkey.jceks")
//                .put("type", "jceks")
//                .put("password", config.getString("secret")));
        JWTAuthOptions jwtConfig = new JWTAuthOptions()
                .setKeyStore(new KeyStoreOptions()
                        .setPath("jwtkey.jceks")
                        .setPassword(config.getString("secret"))
                );
        jwtAuth = JWTAuth.create(vertx, jwtConfig);
    }

    public Future<User> getAccessToken(String clientId, String code, String redirectUri) {
        Future<User> accessToken = Future.future();
        HttpClientOptions options = new HttpClientOptions();
//                .setProxyOptions(new ProxyOptions().setType(ProxyType.HTTP).setHost("10.10.126.46").setPort(8080).setUsername("sc05266"));

        JsonObject tokenConfig = new JsonObject()
                .put("code", code)
                .put("redirect_uri", redirectUri);

        GoogleAuth.create(vertx, clientId, config.getString("secret"), options).authenticate(tokenConfig, res -> {
            if (res.succeeded()) {
                accessToken.complete(res.result());
            } else {
                logger.error(new StringBuilder("Access Token Error: ").append(res.cause().getMessage()));
                accessToken.fail(res.cause());
            }
        });

        return accessToken;
    }

    public Future<JsonObject> getUser(User token) {
        Future<JsonObject> user = Future.future();
        client.get(443, "www.googleapis.com", "/userinfo/v2/me", resp -> {
            if (resp.statusCode() == 200) {
                Buffer totalBuffer = Buffer.buffer();
                resp.handler(buffer -> totalBuffer.appendBuffer(buffer)).endHandler(v -> user.complete(totalBuffer.toJsonObject()));
            } else {
                logger.error(new StringBuilder("Failed retrieving user: ").append(resp.statusCode()).append(" ").append(resp.statusMessage()));
                user.fail(resp.statusMessage());
            }
        }).exceptionHandler(e -> user.fail(e.getMessage())).putHeader("Authorization", new StringBuilder("Bearer ").append(token.principal().getString("access_token"))).end();
        return user;
    }

    public Future<JsonObject> getContact(User token, JsonObject user) {
        Future<JsonObject> contact = Future.future();
        client.get(443, "www.googleapis.com", "/admin/directory/v1/users/" + user.getString("email") + "?viewType=domain_public&projection=full", resp -> {
            if (resp.statusCode() == 200) {
                Buffer totalBuffer = Buffer.buffer();
                resp.handler(buffer -> totalBuffer.appendBuffer(buffer)).endHandler(v -> contact.complete(totalBuffer.toJsonObject()));
            } else {
                logger.error("Failed retrieving contact: " + resp.statusCode() + " " + resp.statusMessage());
                contact.fail(resp.statusMessage());
            }
        }).exceptionHandler(e -> contact.fail(e.getMessage())).putHeader("Authorization", new StringBuilder("Bearer ").append(token.principal().getString("access_token"))).end();
        return contact;
    }

    public Future<String> getJWTToken(JsonObject user, JsonObject contact) {
        Future<String> jwtToken = Future.future();
        JsonObject jwtUser = new JsonObject()
                .put("name", user.getString("name"))
                .put("googleId", user.getString("id"))
                .put("uid", contact.getJsonObject("customSchemas").getJsonObject("ldapUserData").getString("uid"));
        io.vertx.ext.jwt.JWTOptions jwtOptions = new io.vertx.ext.jwt.JWTOptions()
                .setExpiresInMinutes(config.getInteger("expiresIn"))
                .setIssuer(user.getString("hd"))
                .setSubject(user.getString("email"));
        //secret key
        //o4i6AD22vSaHuOdPYsrFdqgk4/rD1SXGfYR6piCif17vebL1qU+rCc8dI3LJUVpIkDvKEaSwUVlM5wAXo+vVzkHjlkxh3qYLBgb8YfnaVNJnnj1/TgtLRbEZNI9fPeGM+mIxEr3NXveh9RAf4Nv7DtOjzUySNI5zXIoovwpXriV96Qy1WlBFmI13WGY01foeS0R1nfVwNwYePalUfAWdAMr37zulGk5T567wQTTT1ui7b/aTCX9d9wgvBZ3U+zpdJClTtqHtaGu72VCig4n16CM19CyJYst2JQHtczWALVqDz6w450AhoZFV+G0EM0cKWJp7rijeilr4qvLNsRA5fA==
        jwtToken.complete(jwtAuth.generateToken(jwtUser, jwtOptions));
        return jwtToken;
    }
}
