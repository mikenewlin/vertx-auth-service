package com.bbva.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Created by sc05266 on 10/17/2017.
 */
public class MainVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        logger.info(new StringBuilder("Starting an instance with PID ").append(Thread.currentThread().getName()));
        deployServiceVerticle().setHandler(resp -> {
            if (resp.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(resp.cause());
            }
        });
    }

    private Future<String> deployServiceVerticle() {
        Future<String> deploy = Future.future();
        vertx.deployVerticle("java-guice:" + ServiceVerticle.class.getName(), new DeploymentOptions().setConfig(config()), deploy.completer());
        return deploy;
    }
}
