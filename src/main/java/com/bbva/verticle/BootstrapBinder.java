package com.bbva.verticle;

import com.bbva.business.AuthService;
import com.bbva.business.impl.AuthServiceImpl;
import com.bbva.handlers.AuthApi;
import com.bbva.handlers.impl.AuthApiImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import javax.inject.Named;

/**
 * Created by sc05266 on 4/28/2017.
 */
public class BootstrapBinder extends AbstractModule {

    @Override
    protected void configure() {
        bind(Vertx.class).toInstance(Vertx.currentContext().owner());
        bind(AuthApi.class).to(AuthApiImpl.class);
        JsonObject jwt = Vertx.currentContext().config().getJsonObject("jwt");
        bind(JsonObject.class).annotatedWith(Names.named("jwt")).toInstance(jwt);
    }

    @Provides
    AuthService provideAuthService(Vertx vertx, @Named("jwt") JsonObject config) {
        return new AuthServiceImpl(vertx, config);
    }

}
