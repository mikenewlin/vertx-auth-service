package com.bbva.verticle;

/**
 * Created by sc05266 on 3/30/2017.
 */
import com.bbva.handlers.AuthApi;
import io.vertx.core.*;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import javax.inject.Inject;

public class ServiceVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    @Inject private AuthApi auth;

    @Override
    public void start(Future<Void> fut) {
        String hostname = System.getenv("HOSTNAME");
        logger.info(new StringBuilder("Starting an instance with PID ").append(Thread.currentThread().getName()));
        vertx.eventBus().consumer("/test/message", handler -> {
            logger.info(new StringBuilder("Received: /test/message - Thread ").append(Thread.currentThread().getName()));
            handler.reply(hostname + " service message ok - " + Thread.currentThread().getName());
        });
        fut.complete();
    }

}
