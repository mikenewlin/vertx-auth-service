package com.bbva.handlers;

import io.vertx.ext.web.RoutingContext;

/**
 * Created by sc05266 on 10/16/2017.
 */
public interface AuthApi {

    void authGoogle(RoutingContext routingContext);
    void getProfile(RoutingContext routingContext);
}
