package com.bbva.handlers.impl;

import com.bbva.business.AuthService;
import com.bbva.handlers.AuthApi;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;

/**
 * Created by sc05266 on 10/16/2017.
 */
public class AuthApiImpl implements AuthApi {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    private AuthService auth;

    @Inject
    public AuthApiImpl(AuthService auth) {
        this.auth = auth;
    }

    public void authGoogle(RoutingContext routingContext) {
        logger.info(new StringBuilder("Auth API Handler: ").append(Thread.currentThread().getId()).append(" ").append(routingContext.request().uri()));
        JsonObject json = routingContext.getBodyAsJson();
        logger.info("Client Id: " + json.getString("clientId"));
        logger.info("Code: " + json.getString("code"));
        logger.info("RedirectUri: " + json.getString("redirectUri"));
        logger.info("State: " + json.getString("state"));
        Future<Void> errorFuture = Future.future();
        errorFuture.setHandler(resp -> {
            if (resp.failed()) {
                logger.error(resp.cause().getMessage());
                routingContext.response().setStatusCode(500).end();
            }
        });
        auth.getAccessToken(json.getString("clientId"), json.getString("code"), json.getString("redirectUri")).compose(v ->
            auth.getUser(v).compose(v1 -> auth.getContact(v, v1).compose(v2 -> auth.getJWTToken(v1, v2).setHandler(jwtToken -> {
                if (jwtToken.succeeded()) {
                    errorFuture.complete();
                    routingContext.response().end(new JsonObject().put("token", jwtToken.result()).encode());
                } else {
                    errorFuture.fail(jwtToken.cause().getMessage());
                }
            })
        , errorFuture), errorFuture), errorFuture);
    }

    public void getProfile(RoutingContext routingContext) {
        User u = routingContext.user();
        String acceptableContentType = routingContext.getAcceptableContentType();
        routingContext.response().putHeader("Content-Type", acceptableContentType);
        routingContext.response().end(new JsonObject()
                .put("name", u.principal().getString("name"))
                .put("email", u.principal().getString("sub"))
                .put("employeeId", u.principal().getString("uid"))
                .encode()
        );
    }
}
